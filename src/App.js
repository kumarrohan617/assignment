import React from 'react'
import LandingPage from './pages/landing-page/landing-page';
import { ChakraProvider } from '@chakra-ui/react'
import { Router } from "@reach/router"
import { AppConstants } from './utils/constants'
import { NewTaskRegistrationForm } from './pages/new-task-registration-form/new-task-registration-form';
function App() {
  return (
    <ChakraProvider>
      <Router>

        <LandingPage path={AppConstants.PATH.LANDING_PAGE} />
        <NewTaskRegistrationForm path={AppConstants.PATH.NEW_TASK_REGISTRATION_PAGE} />


      </Router>

    </ChakraProvider>
  );
}

export default App;
