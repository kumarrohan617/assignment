import { AppConstants } from "../utils/constants"


export const create = (data) => {
  return {
    type: AppConstants.OPERATIONS.CREATE,
    payload: data,
  }
}

export const deleteItem = (data, index) => {
  return {
    type: AppConstants.OPERATIONS.DELETE,
    payload: data,
    index,
  }
}

export const updateItem = (taskList, index, data) => {
  return {
    type: AppConstants.OPERATIONS.UPDATE,
    payload: taskList,
    index,
    data
  }

}

export const updateStatus = (taskList) => {
  return {
    type: AppConstants.OPERATIONS.UPDATING_THE_STATUS_WHEN_DATE_IS_PASSED,
    payload: taskList,
  }

}

