import React from 'react';
import {
  FormControl,
  FormLabel,
} from "@chakra-ui/react"
import { Text } from "@chakra-ui/react"
function SelectThemeInput({ name, type, label, placeholder, errorMsg, register, option, validationObject, errors, isRequired, onChange, defaultValue, ...rest }) {

  return (
    <FormControl id={name} isRequired={isRequired}>
      <FormLabel>{label}</FormLabel>
      <select {...rest} id={name} type={type} placeholder={placeholder} {...register(name, validationObject)} style={{ border: '1px solid black', 'borderRadius': '6px', width: "100%", padding: ".5em 1em" }} onChange={onChange} defaultValue={defaultValue} >
        {(option || []).map((item, index) => <option key={index} value={item['key']}>{item['label']}</option>)}
      </select>
      {errors[name] && <Text mt=".5em" color="red">{errors[name].message}</Text>}
    </FormControl>
  );
}

export default SelectThemeInput;