import React from 'react';
import {
  FormControl,
  FormLabel,
} from "@chakra-ui/react"
import { Text } from "@chakra-ui/react"
function ThemeInput({ name, type, label, placeholder, errorMsg, register, option, validationObject, errors, isRequired, onChange, defaultValue, ...rest }) {

  return (
    <FormControl id={name} isRequired={isRequired}>
      <FormLabel>{label}</FormLabel>
      <input {...rest} option={option} id={name} type={type} placeholder={placeholder} {...register(name, validationObject)} style={{ border: '1px solid black', 'borderRadius': '6px', width: "100%", padding: ".5em 1em" }} onChange={onChange} value={defaultValue} />
      {errors[name] && <Text mt=".5em" color="red">{errors[name].message}</Text>}
    </FormControl>
  );
}

export default ThemeInput;