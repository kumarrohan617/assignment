import { Flex } from '@chakra-ui/layout';
import React from 'react';

function BoxComponent({ label, ...rest }) {
  return (
    <Flex {...rest} >
      {label}
    </Flex>
  );
}

export default BoxComponent;