import { Flex, Stack } from '@chakra-ui/layout';
import React, { useEffect } from 'react';
import BoxComponent from '../../components/reusable-components/box-component';
import NewTaskTable from './new-task-table';
import { AppConstants } from '../../utils/constants';
import { useNavigate } from '@reach/router';
import { useSelector, useDispatch } from 'react-redux';
import { deleteItem, updateStatus } from '../../action/index'
function LandingPage(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const taskList = useSelector(state => state.taskReducer)

  //table heading Array
  const tableHeadingArray = [
    {
      headingLabel: "Name"
    },
    {
      headingLabel: "Task"
    },
    {
      headingLabel: "Status"
    },
    {
      headingLabel: "Date/Time to complete task"
    },
    {
      headingLabel: "Action"
    },

  ]

  useEffect(() => {
    Array.isArray(taskList) && taskList.length > 0 && dispatch(updateStatus(taskList))
  }, [])

  return (
    <Stack border="2px solid black" w="100%" h="100vh" pb="1em">
      <BoxComponent w="15%" label={"TODO APP"} border="2px solid black" justify="center" align="center" fontWeight={'500'} p='1em' ml="2em" mt="2em" mb="6em" />

      <Flex p="1em" justifyContent="flex-end" alignItems="center" pb="2em">
        <BoxComponent cursor="pointer" color="blue" _hover={{ color: 'black', "text-decoration": 'underline' }} onClick={() => navigate(`${AppConstants.PATH.NEW_TASK_REGISTRATION_PAGE}`)} p='1em' mr="4em" border="2px solid black" label={"Create New Task"} fontWeight="500" />
      </Flex>
      <NewTaskTable variant='striped' colorScheme='teal' taskList={taskList} tableHeadingArray={tableHeadingArray} dispatch={dispatch} deleteItem={deleteItem} navigate={navigate} AppConstants={AppConstants} />
    </Stack>
  );
}

export default LandingPage;