import React from 'react'
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Flex,
  Stack,
} from '@chakra-ui/react'
import { EditIcon, DeleteIcon } from '@chakra-ui/icons'
import { getDateFormatter } from '../../utils/helper'
export default function NewTaskTable({ tableHeadingArray, tableBodyArray, taskList, dispatch, deleteItem, navigate, AppConstants, ...rest }) {


  return (
    <Flex px="2em">
      <Table {...rest}>
        <Thead>
          <Tr>
            {Array.isArray(tableHeadingArray) && tableHeadingArray.length > 0 && tableHeadingArray.map((item, index) => {
              return <Th key={index}>
                <Flex justifyContent="center" align="center">{item['headingLabel']}</Flex>
              </Th>
            })}
          </Tr>
        </Thead>
        <Tbody>
          {
            Array.isArray(taskList) && taskList.length > 0 ? taskList.map((item, index) => {
              return < Tr key={index} >
                <Td>
                  <Flex justifyContent="center" align="center">{item['name']}</Flex>
                </Td>
                <Td>
                  <Flex justifyContent="center" align="center">{item['task']}</Flex>
                </Td>
                <Td>
                  <Flex justifyContent="center" align="center">{item['status']}</Flex>
                </Td>
                <Td>
                  <Flex justifyContent="center" align="center">{item['dateAndTimeToCompleteTask'] ? getDateFormatter(item['dateAndTimeToCompleteTask'], 'MMMM Do YYYY, h:mm:ss a') : "NA"}</Flex>
                </Td>
                <Td>
                  <Stack direction="row" justifyContent="center" align="center">
                    <EditIcon cursor="pointer" onClick={() => {
                      navigate(`${AppConstants.PATH.NEW_TASK_REGISTRATION_PAGE}`, {
                        state: {
                          taskList,
                          index,
                          item,
                          isEdit: true
                        }
                      })
                    }} />
                    <DeleteIcon cursor="pointer" onClick={() => {
                      dispatch(deleteItem(taskList, index))
                    }} />
                  </Stack >
                </Td>
              </Tr>
            }) : <Tr>
              <Td colSpan="5"><Flex ml="2.3em" alignItems="center">{"Opps!!No Task found"}</Flex></Td>

            </Tr>
          }


        </Tbody>
      </Table>
    </Flex >
  )
}
