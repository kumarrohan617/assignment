import { Button } from '@chakra-ui/button'
import { Flex, Heading, Stack } from '@chakra-ui/layout'
import React from 'react'
import SelectThemeInput from '../../components/forms/selectThemeInput'
import ThemeInput from '../../components/forms/themeInput'
import { AppConstants } from '../../utils/constants'
import { useForm } from "react-hook-form";
import { useNavigate } from '@reach/router';
import { useDispatch } from "react-redux"
import { create, updateItem } from '../../action/index'

export function NewTaskRegistrationForm(props) {
  const dispatch = useDispatch();

  const { register, handleSubmit, formState: { errors } } = useForm(
    {
      mode: 'all',
      reValidateMode: 'onChange',
      defaultValues: props?.location?.state.isEdit ? Object.keys(props?.location?.state.item).length > 0 ? props?.location?.state.item : {} : {}
    }
  );
  const navigate = useNavigate()

  const onSubmit = (data) => {
    if (props?.location?.state.isEdit) {
      dispatch(updateItem(props?.location?.state.taskList, props?.location?.state.index, data))
    } else { dispatch(create(data)) }

    navigate(`${AppConstants.PATH.LANDING_PAGE}`)
  }
  return (
    <Flex  h="100%" flex={1} justify="center" align="center" borderRadius="6px">
      <Stack spacing={4} border="1px solid black" w="40%" mt="5em" px="1em" borderRadius="6px" py=".5em">
        <Heading>Create A New Task</Heading>
        <Stack border="1px solid black" as={'form'} onSubmit={handleSubmit(onSubmit)}
          noValidate="noValidate"
          p="2em"
          width="100%"
          mt="2em"
          borderRadius="6px"
        >
          <ThemeInput
            name="name"
            label="Name"
            type="text"
            register={register}
            validationObject={{ required: 'Please fill the Details' }}
            errors={errors}
            isRequired
          />

          <ThemeInput
            name="task"
            label="Task"
            type="text"
            register={register}
            validationObject={{ required: 'Please fill the Details' }}
            errors={errors}
            isRequired
          />

          <SelectThemeInput
            name="status"
            label="Status"
            placeholder="Select an option"
            type="select"
            option={[
              {
                key: "",
                label: "Select an option"
              },
              {
                key: "PENDING",
                label: "Pending"
              },
              {
                key: "DONE",
                label: "Done"
              },
              {
                key: "MISSED",
                label: "Missed"
              },
              {
                key: "CANCELLED",
                label: "Cancelled"
              },
            ]}
            register={register}
            validationObject={{ required: 'Please fill the Details' }}
            errors={errors}
            isRequired
          />


          <Stack>
            <ThemeInput
              name="dateAndTimeToCompleteTask"
              label="Date/Time to complete task"
              type="datetime-local"
              register={register}
              validationObject={{ required: 'Please fill the Details' }}
              errors={errors}
              isRequired
            />
          </Stack>
          <Stack direction="row">
            <Button type="submit" w="30%">{props?.location?.state.isEdit ? 'Update' : 'Create'}</Button>
            <Button onClick={() => {
              navigate(AppConstants.PATH.LANDING_PAGE)
            }} type="button" w="30%">{'Back'}</Button>

          </Stack>
        </Stack>
      </Stack>

    </Flex>
  )
}
