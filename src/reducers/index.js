import { combineReducers } from "redux";

import taskReducer from "./task-reducer";

const allReducers = combineReducers({
  taskReducer,
})

export default allReducers;