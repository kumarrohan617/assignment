import { AppConstants } from "../utils/constants";
import { getTimeStamp } from "../utils/helper";


const taskReducer = (state = [], action) => {
  switch (action.type) {
    case AppConstants.OPERATIONS.CREATE: return [action.payload, ...state];

    case AppConstants.OPERATIONS.DELETE:
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        let arrayAfterDeletion = action.payload.filter((item, _index) => _index !== action.index)
        return arrayAfterDeletion
      };
      break;
    case AppConstants.OPERATIONS.UPDATE:
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        let tempArray = [...action.payload]
        tempArray[action.index] = action.data;
        return [...tempArray]
      };
      break;
    case AppConstants.OPERATIONS.UPDATING_THE_STATUS_WHEN_DATE_IS_PASSED:
      if (Array.isArray(action.payload) && action.payload.length > 0) {
        let tempTaskList = []
        tempTaskList = action.payload.map((item, index) => {
          let currentDateTimeStamp = getTimeStamp(new Date())
          let dateAndTimeToCompleteTaskTimeStamp = getTimeStamp(item['dateAndTimeToCompleteTask'])
          if (item['status'] === AppConstants.STATUS.PENDING && currentDateTimeStamp > dateAndTimeToCompleteTaskTimeStamp) {
            item['status'] = AppConstants.STATUS.MISSED;
          }
          return item;
        })
        return [...tempTaskList];

      }
      break;
    default:
      return state;
  }

}

export default taskReducer;