export let AppConstants = {
  STATUS: {
    PENDING: 'PENDING',
    MISSED: "MISSED"
  },
  PATH: {
    LANDING_PAGE: "/",
    NEW_TASK_REGISTRATION_PAGE: "/new-task-registration-page"
  },
  OPERATIONS: {
    CREATE: "CREATE",
    DELETE: "DELETE",
    UPDATE: "UPDATE",
    UPDATING_THE_STATUS_WHEN_DATE_IS_PASSED: "UPDATE_STATUS",
  }
}