import moment from 'moment'
export const getTimeStamp = (date) => {
  let timeStamp = moment(date).valueOf()
  if (timeStamp > 0) {
    return timeStamp
  } else {
    return ""
  }
}

export const getDateFormatter = (date, format) => {
  let forMattedDate = moment(date).format(format)
  if (forMattedDate) {
    return forMattedDate
  } else {
    return ""
  }
}